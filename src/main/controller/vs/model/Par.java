package vs.model;

public class Par {
	private String ferfiNev;
	private String noiNev;
	private int sorokSzama;
	private int szabadIdo;
	final static int ROSSZMECCSSORDB = 1;
	final static int JOMECCSSORDB = 3;

	public Par(String ferfiNev, String noiNev) {
		super();
		this.ferfiNev = ferfiNev;
		this.noiNev = noiNev;
	}

	public void meccs(int szIdoNovekmeny, boolean jomeccs) {

		if (jomeccs) {
			sorokSzama += JOMECCSSORDB;
		} else {
			sorokSzama += ROSSZMECCSSORDB;
		}
		szabadIdo += szIdoNovekmeny;

	}

	public String getFerfiNev() {
		return ferfiNev;
	}

	public void setFerfiNev(String ferfiNev) {
		this.ferfiNev = ferfiNev;
	}

	public String getNoiNev() {
		return noiNev;
	}

	public void setNoiNev(String noiNev) {
		this.noiNev = noiNev;
	}

	public int getSorokSzama() {
		return sorokSzama;
	}

	public void setSorokSzama(int sorokSzama) {
		this.sorokSzama = sorokSzama;
	}

	public int getSzabadIdo() {
		return szabadIdo;
	}

	public void setSzabadIdo(int szabadIdo) {
		this.szabadIdo = szabadIdo;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Parok : ");
		builder.append(ferfiNev);
		builder.append(" + ");
		builder.append(noiNev);
		builder.append("sorokSzama : ");
		builder.append(sorokSzama);
		builder.append("SzabadIdo : ");
		builder.append(szabadIdo);
		return builder.toString();
	}

}
