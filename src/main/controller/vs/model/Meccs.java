package vs.model;

import java.util.ArrayList;

public class Meccs {
	
	private String csapat1;
	private String csapat2;
	private int meccshossz;
	private String minoseg;
	private int nezoszam;
	private ArrayList<Par> nezok;
	


	public Meccs(String csapat1, String csapat2, int meccshossz, String minoseg, int nezoszam, ArrayList<Par> nezok) {
		super();
		this.csapat1 = csapat1;
		this.csapat2 = csapat2;
		this.meccshossz = meccshossz;
		this.minoseg = minoseg;
		this.nezoszam = nezoszam;
		this.nezok = nezok;
	}

	public String getCsapat1() {
		return csapat1;
	}

	public void setCsapat1(String csapat1) {
		this.csapat1 = csapat1;
	}

	public String getCsapat2() {
		return csapat2;
	}

	public void setCsapat2(String csapat2) {
		this.csapat2 = csapat2;
	}

	public int getMeccshossz() {
		return meccshossz;
	}

	public void setMeccshossz(int meccshossz) {
		this.meccshossz = meccshossz;
	}

	public String getMinoseg() {
		return minoseg;
	}

	public void setMinoseg(String minoseg) {
		this.minoseg = minoseg;
	}

	public int getNezoszam() {
		return nezoszam;
	}

	public void setNezoszam(int nezoszam) {
		this.nezoszam = nezoszam;
	}

	public ArrayList<Par> getNezok() {
		return nezok;
	}

	public void setNezok(ArrayList<Par> nezok) {
		this.nezok = nezok;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(csapat1);
		builder.append(" - ");
		builder.append(csapat2);
		builder.append(" - ");
		builder.append(meccshossz);
		builder.append(" perc.");
		return builder.toString();
	}
	
	
	

}
