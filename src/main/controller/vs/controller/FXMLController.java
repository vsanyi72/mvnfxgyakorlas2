package vs.controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import vs.dao.FileDao;
import vs.model.Csapat;
import vs.model.Meccs;
import vs.model.Par;

public class FXMLController implements Initializable {

	ArrayList<Csapat> csapatok = new ArrayList<>();
	ArrayList<Par> parok = new ArrayList<>();
	ArrayList<Meccs> meccsek = new ArrayList<>();
	ArrayList<Par> nezok = new ArrayList<>();
	int osszesMeccs = 0;
	int nezoSzam = 0;

	
	@FXML
	private Label lbSor;

	@FXML
	private Label lbPerc;

	@FXML
	private Label lbCsapatok;

	@FXML
	private TextArea taHazasparok;

	@FXML
	private TextArea taFerjek;

	@FXML
	private TextArea taFelesegek;

	@FXML
	private TextArea taSzabalyzat;

	@FXML
	private ImageView ivFoci;

	@FXML
	private Button btnMeccs;

	@FXML
	public Button btnMentes;

	@FXML
	public TabPane tpAblak;

	@FXML
	private TableView<Meccs> tvLista;

	@FXML
	private TableColumn<Meccs, String> tcCsapat1;

	@FXML
	private TableColumn<Meccs, String> tcCsapat2;

	@FXML
	private TableColumn<Meccs, String> tcMeccsHossz;

	@FXML
	private TableColumn<Meccs, String> tcMinoseg;

	@FXML
	private TableColumn<Meccs, String> tcNezoSzam;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		FileDao fd = new FileDao();
		try {
			csapatok = fd.readCsapat("d:\\csapatok.txt");
			parok = fd.readPar("d:\\parok.txt");
			refreshTexts();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
// init Szabalyok tab
		try {
			String szabalyzat = "";
			BufferedReader br = new BufferedReader(new FileReader(new File("d://szabalyzat.txt")));
			String sor;
			while ((sor = br.readLine()) != null) {
				szabalyzat += sor + "\n\r";
			}
			taSzabalyzat.setText(szabalyzat);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// mouse textArea event listener
		taHazasparok.setOnMouseClicked(event -> {
			if (event.getButton() == MouseButton.PRIMARY) {

				// check, if click was inside the content area
				Node n = event.getPickResult().getIntersectedNode();
				while (n != taHazasparok) {
					if (n.getStyleClass().contains("content")) {
						// find previous/next line break
						int caretPosition = taHazasparok.getCaretPosition();
						String text = taHazasparok.getText();
						int lineBreak1 = text.lastIndexOf('\n', caretPosition - 1);
						int lineBreak2 = text.indexOf('\n', caretPosition);
						if (lineBreak2 < 0) {
							// if no more line breaks are found, select to end of text
							lineBreak2 = text.length();
						}

						taHazasparok.selectRange(lineBreak1, lineBreak2);
						event.consume();
						try {
							nezettMeccsekKiiras();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						break;
					}
					n = n.getParent();

				}

			}

		});

	}

	public void nezettMeccsekKiiras() throws IOException {
		String kivalasztottPar = taHazasparok.getSelectedText().substring(1);
		String ferfi = kivalasztottPar.split(" ")[0];
		String no = kivalasztottPar.split(" ")[2];
		kivalasztottPar=ferfi+no;
		Stage stage2 = new Stage();
		{
			stage2.setTitle("Meccsek");
			TextArea taMeccsek = new TextArea();
			AnchorPane ap = new AnchorPane(taMeccsek);
			Scene scene = new Scene(ap, 300, 300);
			stage2.setScene(scene);
			stage2.show();
			ArrayList<Par> nezoParok = new ArrayList<>();
			String parTmp="";
			String meccsekKiirni="";
			for (Meccs meccs : meccsek) {
				nezoParok = meccs.getNezok();
				for (Par par : nezoParok) {
					parTmp = par.getFerfiNev()+par.getNoiNev();
					System.out.println(parTmp+ "--"+kivalasztottPar);
					if (kivalasztottPar.equals(parTmp)) {
						
						meccsekKiirni+=meccs+"\n\r";
					}
				}
			}
			taMeccsek.appendText(meccsekKiirni);
					System.out.println(meccsekKiirni);
			stage2.setOnCloseRequest((event) -> {
			//	System.out.println("Closing Stage");
			});
		}

	}

	public void pressedMeccsBtn(ActionEvent event) {
		final int MECCSHOSSZ = 90;
		Random rnd = new Random();
		String csapat1 = "";
		String csapat2 = "";
		csapat1 = csapatok.get(rnd.nextInt(csapatok.size())).getNev();
		while ((csapat2 = csapatok.get(rnd.nextInt(csapatok.size())).getNev()).equals(csapat1)) {
		}
		lbCsapatok.setText(csapat1 + " , " + csapat2 + " :-)");
		int teljesMeccsHossz = MECCSHOSSZ + rnd.nextInt(20);
		String minoseg = "Rossz";
		boolean joMeccs = rnd.nextBoolean();
		if (joMeccs) {
			minoseg = "Jó";
		}
		osszesMeccs++;

		for (Par par : parok) {
			if (rnd.nextBoolean()) {
				par.meccs(teljesMeccsHossz, joMeccs);
				nezok.add(par);
				nezoSzam++;
			}
		}
		refreshTexts();
		int sorAtlag = 0;
		int osszSzabadIdo = 0;
		for (Par par : parok) {
			sorAtlag += par.getSorokSzama();
			osszSzabadIdo += par.getSzabadIdo();
		}

		sorAtlag = sorAtlag / osszesMeccs;

		lbPerc.setText("Összesen " + osszSzabadIdo + " perc.");
		lbSor.setText("Átlagosan " + sorAtlag + " sor fogyott.");
		Meccs meccs = new Meccs(csapat1, csapat2, teljesMeccsHossz, minoseg, nezoSzam, nezok);
		meccsek.add(meccs);
		refreshTable();

	}

	public void refreshTexts() {
		String kiirniFerfi = "";
		String kiirniParok = "";
		String kiirniNo = "";
		for (Par par : parok) {
			kiirniFerfi += par.getFerfiNev() + " " + par.getSorokSzama() + " sört ivott.\n\r";
			kiirniNo += par.getNoiNev() + " szabadideje: " + par.getSzabadIdo() + " perc.\n\r";
			kiirniParok += par.getFerfiNev() + " - " + par.getNoiNev() + "\n\r";
		}
		taFerjek.clear();
		taFelesegek.clear();
		taHazasparok.clear();
		taFerjek.appendText(kiirniFerfi);
		taFelesegek.appendText(kiirniNo);
		taHazasparok.appendText(kiirniParok);
	}

	public void refreshTable() {
		tcCsapat1.setCellValueFactory(new PropertyValueFactory<>("csapat1"));
		tcCsapat2.setCellValueFactory(new PropertyValueFactory<>("csapat2"));
		tcMeccsHossz.setCellValueFactory(new PropertyValueFactory<>("meccshossz"));
		tcMinoseg.setCellValueFactory(new PropertyValueFactory<>("minoseg"));
		tcNezoSzam.setCellValueFactory(new PropertyValueFactory<>("nezoszam"));
		ObservableList<Meccs> meccsLista = FXCollections.observableArrayList();
		meccsLista.addAll(meccsek);
		tvLista.getItems().clear();
		tvLista.setItems(meccsLista);

	}

	public void mentes() {
		System.out.println("Mentes");
		Stage fileStage = new Stage();
		fileStage.setTitle("Meccs adatok mentése");
		// stage.setScene(scene);
		fileStage.show();
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Meccs adatok mentése");
		fileChooser.getExtensionFilters().addAll(new ExtensionFilter("All Files", "*.txt"));
		File file = fileChooser.showSaveDialog(fileStage);
		fileStage.close();

		BufferedWriter bw;
		try {
			bw = new BufferedWriter(new FileWriter(file));
			for (Meccs meccs : meccsek) {
				bw.write(meccs.getCsapat1() + ";" + meccs.getCsapat2() + ";" + meccs.getMeccshossz() + ";"
						+ meccs.getMinoseg() + ";" + meccs.getNezoszam() + "\n\r");
			}
			bw.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
