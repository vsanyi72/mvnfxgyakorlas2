package vs.dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import vs.model.Csapat;
import vs.model.Par;

public class FileDao implements Dao {

	@Override
	public ArrayList<Csapat> readCsapat(String path) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(new File(path)));
		String nev;
		ArrayList<Csapat> csapatok = new ArrayList<>();
		while ((nev = br.readLine()) != null) {
			Csapat csapat = new Csapat(nev);
			csapatok.add(csapat);
		}
		br.close();
		return csapatok;
	}

	@Override
	public ArrayList<Par> readPar(String path) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(new File(path)));
		String ferfiNev;
		String noiNev;
		String sor;
		ArrayList<Par> parok = new ArrayList<>();
		while ((sor = br.readLine()) != null) {
			ferfiNev = sor.split(";")[0];
			noiNev = sor.split(";")[1];
			Par par = new Par(ferfiNev, noiNev);
			parok.add(par);
		}
		br.close();
		return parok;
	}

}
